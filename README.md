# Projet agilité : "Gérer des indicateurs de santé"
Site accessible en ligne ici : https://decatlon-indicateur.herokuapp.com  
Backlogs & board accessible ici : https://gitlab.com/drobaey/agility-project/-/boards  
Version finale accessible via le tag "v1.0" ou le dernier commit.

## Membres de l'équipe "Challenge_accepted"
- ROBAEY Dennis
- VANDEWALLE Charles
- Neima Mohamed Abdoulkader
- ABAYAZID Youssouf

# Compte rendu des différents sprints :

## 1er sprint :
On a d'abord fait les backlogs, on s’est mis d’accord sur les langages à utiliser, puis on s’est répartie les tâches.

### Sprint planning : on s'est répartie les tâches de la manière suivante : (1 personne pour chaque tâche)  

- Faire la page html de l'accueil du site
- Faire les différentes pages en html
- Essayer de gérer le déploiement en ligne
- Commencer la bdd (choisir quelles tables utiliser, la créer)

### Démo : 
Principalement le début du rendu des pages html, vu que rien n'était un minimum fini.
### Rétro: 
Mieux définir les tâches de la backlog en début de projet, faire un board de la backlog, et en choisir à chaque début de sprint.


## 2e sprint : 

### Sprint planning :
2 d’entre nous ont continués sur les pages html du site (des pages différentes). Ensuite les 2 autres ont travaillés sur : 
- Le déploiement en ligne de manière continue + sonarqube (non fonctionnel).
- Organisation de la base de données et mise en place de celle-ci (lien entre la BDD en local mais aussi pour le site en ligne)

### Démo :
Visualisation du design des pages d'accueil et de connexion, bref explication bdd et déploiement en ligne (non fonctionnel)  
### Rétro : 
Difficulté avec la base de données. Il faut s’assurer d’avoir des choses fonctionnelles pour les prochaines démos et prendre plus de temps pour la préparer

## 3e sprint :

### Sprint planning :
3 tâches principales : (1 par membre du groupe, sauf la dernière)
 - Fusion et mise en place des pages html dans le projet (terminé)
- Gérer l'ajout à la base de données avec les formulaires de la page web (non terminé)
 - Tests de la page web sur différentes routes (répartie entre 2 personnes) (non terminé)

### Démo : 
Visualisation du site en ligne qui fonctionne, sans gestion de la BDD, début des tests sur l'interface.

### Rétro : 
Certaines difficultés pour la création des tests, manque de communication (utilisation du site non à jour pour les tests). On devra donc se mettre en binôme pour des tâches complexes, essayer de réduire la longueur des tâches et plus communiquer.

## 4e sprint :

### Sprint planning :
2 tâches principales au début :
 - Gérer l'ajout à la base de données avec les formulaires de la page web (2 personnes travaillent dessus) (terminé)
 - Tests de la page web sur différentes routes (répartie entre 2 personnes) (terminé)

### Démo : 
On a montré le site en ligne qui gérait les formulaires de connexion, d'inscription et le formulaire pour la saisi de données (sans conserver la session). On a montré que les tests intégrés au CI/CD fonctionnaient.  
### Rétro : 
- On a bien avancé, on a bien fini nos tâches respectives et on en a même terminés d'autres.   
- Un membre du groupe a cependant eu un petit problème sur la mise en place de l'intégration des tests mais il en a parlé et on a corrigé ce problème à plusieurs.
- Il faut donc continuer de se répartir des tâches réalisable avant la fin d'un sprint et communiquez dès qu'il y a un soucis.


## 5e sprint :

### Sprint planning :  
4 tâches pour chaque membre du groupes au départ :  
 - Intégration continue avec sonarqube et pyling  (terminé)
 - Message d'erreur et de validation lorsqu'un utilisateur se connecte (terminé)
 - Test de la connexion pour un utilisateur qui existe dans la base de données (on test aussi pour un utilisateur qui n'existe pas)
 - Test de l'interface de la page de saisie (terminé)

### Démo :
On a montré que :
 - les mots de passe sont maintenant cryptés
 - la session de l'utilisateur est maintenant géré
 - un utilisateur ne peut accéder qu'à certaines pages en fonction de s'il est connecté ou non
 - on peut désormais saisir les données/indicateurs de santé et les voirs dans l'historique
 - on peut voir l'historique et modifier les données des jours précédents si besoin
 - on a un peu modifié le style css des pages
 - pylint et sornarcube sont désormais intégré dans la CI/CD
 - on a avancé sur les tests unitaire de l'interface


### Rétro :
 - on a très bien avancé et fait beaucoup plus de tâche que prévu au départ
 - certains problèmes pour les tests
 - travailler en binôme pour certains tests en cas de problème


## 6e sprint (final) :

### Sprint planning :
4 tâches pour chaque membres du groupe au départ :
 - continuer d'améliorer le style des pages
 - gérer plusieurs unité dans le formulaire des données/indicateurs de santé
 - continuer les tests de connexion dans l'interface
 - faire les tests liés à l'inscription dans l'interface

### Démo finale :
On a montré l'ensemble du site fonctionnelle. Le fait que les tests, pylint, sonarqube et le déploiement en ligne sont intégrés à la CI/CD de manière fonctionnelle.